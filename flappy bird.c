#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>

// 全局变量
int high,width; // 游戏画面大小
int bird_x,bird_y; // 小鸟的坐标
int obstacle_y,obstacle_xDown,obstacle_xTop; // 障碍物
int score; // 得分，经过障碍物的个数

void gotoxy(int x,int y)//类似于清屏函数
{
    HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD pos;
    pos.X = x;
    pos.Y = y;
    SetConsoleCursorPosition(handle,pos);
}

void startup()  // 数据初始化
{
	high=20;
	width=20;
	bird_x=high/2;
	bird_y=3;
	obstacle_y=width;
	obstacle_xDown=high/3;
	obstacle_xTop=high/2;
	score = 0;
}

void show()  // 显示画面
{
	gotoxy(0,0);  // 清屏	
	int i,j;
	
	for (i=0;i<high;i++)
	{
		for (j=0;j<width;j++)
		{
			if ((i==bird_x) && (j==bird_y))
				printf("@");  //   输出小鸟
			else if ((j==obstacle_y) && ((i<obstacle_xDown) || (i>obstacle_xTop)) )
				printf("*");  //   输出墙壁
			else
				printf(" ");  //   输出空格
		}
		printf("\n");
	}
	printf("得分:%d\n",score);
}	

void updateWithoutInput()  // 与用户输入无关的更新
{
	bird_x++;
	obstacle_y--;
	if (bird_y==obstacle_y)
	{
		if ((bird_x>=obstacle_xDown)&&(bird_x<=obstacle_xTop))
			score++;
		else
		{
			printf("再来一次\n");
			exit(0);
		}
	}
	if (obstacle_y<=0)  // 再新生成一个障碍物
	{
		int u=(int)high*0.8;
		obstacle_y=width;
		int temp=rand()%u;
		obstacle_xDown=temp-high/10;
		obstacle_xTop=temp+high/10;
	}
	Sleep(150);
}

void updateWithInput()  // 与用户输入有关的更新
{	
	char input;
	if(kbhit())  // 判断是否有输入
	{
		input=getch();  // 根据用户的不同输入来移动，不必输入回车
		if (input==' ')  
		{
			bird_x=bird_x-2;
		}
	}
}

void main()
{
	startup();  // 数据初始化	
	while (1)  //  游戏循环执行
	{
		show();  				// 显示画面
		updateWithoutInput();  // 与用户输入无关的更新
		updateWithInput();    // 与用户输入有关的更新
	}
}


